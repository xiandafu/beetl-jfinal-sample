package com.demo;

import java.io.IOException;
import java.io.Writer;


public class MyNoLockStringWriter extends Writer
{

	StringBuilder buf = new StringBuilder(512);

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException
	{
		buf.append(cbuf, off, len);

	}

	public void write(String str)
	{
		buf.append(str);
	}

	@Override
	public void flush() throws IOException
	{
		System.out.println("flused by....");
	}

	@Override
	public void close() throws IOException
	{
		System.out.println("closed by....");

	}

	public String toString()
	{
		return buf.toString();
	}

}