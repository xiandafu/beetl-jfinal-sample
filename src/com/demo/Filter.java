package com.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * Servlet Filter implementation class Filter
 */
public class Filter implements javax.servlet.Filter {

    /**
     * Default constructor. 
     */
    public Filter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("filter first");
		chain.doFilter(request, new MyServletResponseWrapper( response));
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	

}


class MyServletResponseWrapper extends HttpServletResponseWrapper
{
	MyNoLockStringWriter cw = null;
	PrintWriter writer = null;

	public MyServletResponseWrapper(ServletResponse rsp)
	{
		super((HttpServletResponse)rsp);
	}

	public java.io.PrintWriter getWriter()
	{
		if (writer == null)
		{
			cw = new MyNoLockStringWriter();
			writer = new PrintWriter(cw);
		}
	
		return writer;
	}



}
