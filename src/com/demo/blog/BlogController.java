package com.demo.blog;

import java.util.Enumeration;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

/**
 * BlogController
 */
@Before(BlogInterceptor.class)
public class BlogController extends Controller {
	public void index() {
		Page page  = Blog.dao.paginate(getParaToInt(0, 1), 10, "select *", "from blog order by id asc");
		Blog blog = (Blog)page.getList().get(0);
		Object id = blog.get("id");
		Object title = blog.get("tilte");
		setAttr("blogPage",page );
		setAttr("testList",null);
//		this.getSession().setAttribute("name", "lucy");
//		System.out.println(this.getSession()+"  123");
		
		
		Enumeration en = this.getAttrNames();
		while(en.hasMoreElements()){
			System.out.println(en.nextElement());
		}
		this.setSessionAttr("name", "joelli");
		
		render("blog.html");
		
	}
	
	public void add() {
		//render("add.html");
	}
	
	@Before(BlogValidator.class)
	public void save() {
		getModel(Blog.class).save();
		forwardAction("/blog");
	}
	
	public void edit() {
		setAttr("blog", Blog.dao.findById(getParaToInt()));
//		render("edit.html");
	}
	
	@Before(BlogValidator.class)
	public void update() {
		getModel(Blog.class).update();
		forwardAction("/blog");
	}
	
	public void delete() {
		Blog.dao.deleteById(getParaToInt());
		forwardAction("/blog");
	}
}


